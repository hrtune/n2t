from CompilationEngine import CompilationEngine
import os
import sys
def is_directory(arg):
    if arg[-5:] == ".jack":
        return False
    else:
        return True

def write_xml():
    arg = sys.argv[1]

    if not is_directory(arg):
        obj = CompilationEngine(arg)
        with open(arg[:-5] + ".xml", "w") as f:
            f.write(obj.o)

    else:
        if arg[-1] != "/":
            arg = arg + "/"
        ls = os.listdir(arg)
        #print(ls)
        for e in ls:
            if e[-5:] == ".jack":
                obj = CompilationEngine(arg + e)
                e = e[:-5]
                with open(arg + e + ".xml","w") as f:
                    f.write(obj.o)


def main():
    write_xml()


if __name__ == '__main__':
    main()
