
def del_comment(s):

    while "/*" in s:
        start = s.find("/*")
        end = s.find("*/", start)
        s = s[:start] + " " + s[end + 2:]

    while "//" in s:
        start = s.find("//")
        end = s.find("\n", start)
        s = s[:start] + " " + s[end + 1:]

    s = s.replace("\n", " ")
    s = s.replace("\t", " ")

    s = s + "\t"

    return s



class JackTokenizer:

    keywords = ["class", "constructor", "function", "method",
    "field", "static", "var", "int", "char", "boolean", "void",
    "true", "false", "null", "this", "let", "do", "if", "else", "while",
    "return"]

    symbols = ["{", "}", "(", ")", "[", "]", ".", ",", ";", "+", "-", "*",
    "/", "&", "|", "<", ">", "=", "~"]

    def __init__(self, jack_file):
        self.token = ""
        self.i = 0
        self.output = ""
        with open(jack_file) as f:
            self.jack = f.read()

        self.jack = del_comment(self.jack)

        file_name = jack_file[:-5] + "T" + ".xml"

        with open(file_name, "w") as f:
            f.write("<tokens>\n")

        with open(file_name, "a") as f:
            self.advance()
            while(self.hasMoreTokens()):
                f.write(self.tag())
                self.advance()
            f.write("</tokens>\n")

        self.i = 0
        self.token = ""



    def tag(self):
        if self.tokenType() == "KEYWORD":
            return "<keyword> " + self.token + " </keyword>\n"
        elif self.tokenType() == "SYMBOL":
            token = self.token
            if self.token == "<":
                token = "&lt;"
            elif self.token == ">":
                token = "&gt;"
            elif self.token == "&":
                token = "&amp;"
            return "<symbol> " + token + " </symbol>\n"
        elif self.tokenType() == "STRING_CONST":
            return "<stringConstant> " + self.token[1:-1] + " </stringConstant>\n"
        elif self.tokenType() == "INT_CONST":
            return "<integerConstant> " + self.token + " </integerConstant>\n"
        elif self.tokenType() == "IDENTIFIER":
            return "<identifier> " + self.token + " </identifier>\n"
        else:
            return ""

    def hasMoreTokens(self):
        return not(self.i == -1)

    def tokenType(self):
        t = self.token
        if t in self.symbols:
            return "SYMBOL"
        elif t[0] == "\"" and t[-1] == "\"":
            return "STRING_CONST"
        elif t.isdigit():
            return "INT_CONST"
        elif t in self.keywords:
            return "KEYWORD"
        else:
            return "IDENTIFIER"

    def advance(self):
        t = ""
        i = self.i
        jack = self.jack
        while(True):
            if jack[i] in self.symbols:

                if t:
                    if "\"" in t:
                        t += jack[i]
                    else:
                        self.token = t
                        self.i = i
                        return
                else:
                    self.token = jack[i]
                    self.i = i + 1
                    return

            elif jack[i] == " ":
                if t:
                    if "\"" in t:
                        t += jack[i]
                    else:
                        self.token = t
                        self.i = i + 1
                        return

            elif jack[i] == "\"":
                if "\"" in t:
                    self.token = t + jack[i]
                    self.i = i + 1
                    return
                else:
                    t += jack[i]

            elif jack[i] == "\t":
                if t:
                    self.token = t
                self.i = -1
                return

            else:
                t += jack[i]

            i = i + 1





    def keyWord(self):
        return self.token.upper()

    def symbol(self):
        return self.token

    def identifier(self):
        return self.token

    def intVal(self):
        return int(self.token)

    def stringVal(self):
        return self.token.replace("\"","")





    def printj(self):
        text = self.jack
        print(text)
