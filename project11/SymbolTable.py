class SymbolTable:

    def __init__(self):
        #the first keywords defined as the name of the variables
        # ex. {"x" : {"type": "int", "kind": "field", "index": 0}, ...}
        self.class_table = {}
        self.sub_table = {}

        #indices of each variables
        self.indices = {}

    def define(self, name, type, kind):

        if kind in self.indices:
            self.indices[kind] += 1
        else:
            self.indices[kind] = 0

        if kind == "static":
            self.class_table[name] = {}
            self.class_table[name]["type"] = type
            self.class_table[name]["kind"] = kind
            self.class_table[name]["index"] = self.indices[kind]

        elif kind == "field":
            self.class_table[name] = {}
            self.class_table[name]["type"] = type
            self.class_table[name]["kind"] = "this"
            self.class_table[name]["index"] = self.indices[kind]

        elif kind in ["argument", "local"]:
            self.sub_table[name] = {}
            self.sub_table[name]["type"] = type
            self.sub_table[name]["kind"] = kind
            self.sub_table[name]["index"] = self.indices[kind]
        else:
            raise Exception(kind + " is not excepted kind")

    def start_sub(self):
        self.sub_table = {}

        if "argument" in self.indices:
            del self.indices["argument"]

        if "local" in self.indices:
            del self.indices["local"]


    def kind_of(self, name):
        if name in self.sub_table:
            return self.sub_table[name]["kind"]
        elif name in self.class_table:
            return self.class_table[name]["kind"]
        else:
            return "none"

    def var_count(self, kind):
        if kind in self.indices:
            return self.indices[kind] + 1
        else:
            return 0

    def type_of(self, name):
        if name in self.sub_table:
            return self.sub_table[name]["type"]
        elif name in self.class_table:
            return self.class_table[name]["type"]
        else:
            return ""

    def index_of(self, name):
        if name in self.sub_table:
            return self.sub_table[name]["index"]
        elif name in self.class_table:
            return self.class_table[name]["index"]
