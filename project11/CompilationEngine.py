from JackTokenizer import JackTokenizer
from SymbolTable import SymbolTable
class VMWriter:
    def __init__(self, jack_file):
        file_name = jack_file[:-5] + ".vm"
        with open(file_name, "w") as f:
            f.write("")

        self.f = open(file_name, "a")

    def close(self):
        self.f.close()

    def write_push(self, segment, index):
        self.f.write("push {} {}\n".format(segment, index))

    def write_pop(self, segment, index):
        self.f.write("pop {} {}\n".format(segment, index))

    def write_sa(self, op):
        if op == "-":
            self.f.write("neg\n")
        elif op == "~":
            self.f.write("not\n")
        else:
            raise Exception()

    def write_a(self, op):

        if op == "+":
            command = "add"
        elif op == "-":
            command = "sub"
        elif op == "*":
            command = "call Math.multiply 2"
        elif op == "/":
            command = "call Math.divide 2"
        elif op == "&":
            command = "and"
        elif op == "|":
            command = "or"
        elif op == "<":
            command = "lt"
        elif op == ">":
            command = "gt"
        elif op == "=":
            command = "eq"
        else:
            raise Exception(op)

        self.f.write(command + "\n")

    def write_label(self, label):
        self.f.write("label {}\n".format(label))

    def write_goto(self, label):
        self.f.write("goto {}\n".format(label))

    def write_if(self, label):
        self.f.write("if-goto {}\n".format(label))

    def write_call(self, name, nargs):
        self.f.write("call {} {}\n".format(name, nargs))

    def write_f(self, name, nlocals):
        self.f.write("function {} {}\n".format(name, nlocals))

    def write_r(self):
        self.f.write("return\n")






class CompilationEngine:

    class_name = ""
    label_i = 0
    sub_kind = ""
    sub_name = ""

    def __init__(self, jack_file):
        self.t = JackTokenizer(jack_file)
        self.st = SymbolTable()
        self.w = VMWriter(jack_file)
        self.i = 0
        self.o = ""

        self.t.advance()
        if self.t.token == "class":
            self.compileClass()
        else:
            raise Exception("No \"class\" keyword.")

    def compileClass(self):
        #'class' className {}
        self.o += "<class>\n"
        self.output()

        self.t.advance()
        if self.is_identifier():
            CompilationEngine.class_name = self.t.token

            self.output()

        self.t.advance()
        if self.t.token == "{":
            self.output()

        while True:
            if self.next_token() in ["static", "field"]:
                self.t.advance()
                self.compileClassVarDec()
            else:
                break

        while True:
            if self.next_token() in ["constructor", "function", "method"]:
                self.t.advance()
                print("class :", self.st.class_table)
                print("sub :", self.st.sub_table)
                self.compileSubroutineDec()
            else:
                break

        if self.t.token == "}":
            self.output()

        self.o += "</class>\n"
        print("class :", self.st.class_table)
        print("sub :" , self.st.sub_table)

    def compileClassVarDec(self):
        # ('static' | 'field') type varName (',', varName)* ';'

        if self.t.token in ["constructor", "function", "method", "}"]:
            return
        elif self.t.token not in ["static", "field"]:
            raise Exception("FOO")

        self.o += "<classVarDec>\n"


        kind = self.t.token # ('static' | 'field')

        self.output()

        #type
        self.t.advance()
        if self.is_type():
            self.output()
            type = self.t.token

        #varName
        self.t.advance()
        if self.is_identifier():
            self.output()
            name = self.t.token

        #here,the code has declared one argument at least
        self.st.define(name, type, kind)

        #(, varName)*
        while True:
            #the next token should be "," or ";"
            self.t.advance()
            if self.t.token == ",":
                self.output()
            elif self.t.token == ";":
                self.output()
                break
            else:
                raise Exception("FOO")

            #varName
            self.t.advance()
            if self.is_identifier():
                self.output()
                name = self.t.token
                self.st.define(name, type, kind)
            else:
                raise Exception()

        self.o += "</classVarDec>\n"

    def compileSubroutineDec(self):
        #('constructor' | 'function' | 'method') ('void' | type)
        #'(parameterList)' subroutineBody
        self.o += "<subroutineDec>\n"

        if self.t.token in ["constructor", "function", "method"]:
            self.output()
            CompilationEngine.sub_kind = self.t.token
            self.st.start_sub()

        else:
            self.o += "</subroutineDec>\n"
            return




        #"void" or type
        self.t.advance()
        if self.t.token == "void" or self.is_type():
            self.output()
        else:
            raise Exception("Foo")

        #subroutineName
        self.t.advance()
        if self.is_identifier():
            self.output()
            CompilationEngine.sub_name = self.t.token

        #(parameterList)
        self.aieo("(")

        self.t.advance()
        self.compileParameterList()

        if self.t.token == ")":
            self.output()
        else:
            raise Exception()


        self.t.advance()
        self.compileSubroutineBody()

        self.o += "</subroutineDec>\n"

    def compileParameterList(self):
        if CompilationEngine.sub_kind == "method":
            self.st.define("this", CompilationEngine.class_name, "argument")
        #(type varName) (, type varName)*
        self.o += "<parameterList>\n"
        #the next token should be ")" or type
        if self.t.token == ")":
            self.o += "</parameterList>\n" #There are no parameters
            return



        #type for the first argument
        elif self.is_type():
            self.output()
            type = self.t.token

        #the next token should be varName
        self.t.advance()
        if self.is_identifier():
            self.output()
            name = self.t.token
            self.st.define(name, type, "argument")

        #the next token should be "," or ")"
        while True:

            self.t.advance()
            if self.t.token == ")":
                self.o += "</parameterList>\n"
                return
            elif self.t.token == ",":
                self.output()
            else:
                raise Exception()

            #type
            self.t.advance()
            if self.is_type():
                self.output()
                type = self.t.token

            #varName
            self.t.advance()
            if self.is_identifier():
                self.output()
                name = self.t.token
                self.st.define(name, type, "argument")

    def compileTerm(self):

        temp_t = self.t.token
        temp_i = self.t.i
        self.o += "<term>\n"
        #case varName | varName[expression] | subroutineCall
        if self.is_identifier():
            self.output()
            id = self.t.token

            # varName[]
            self.t.advance()
            if self.t.token == "[":
                self.output()

                kind = self.st.kind_of(id)
                index = self.st.index_of(id)
                self.w.write_push(kind, index)

                self.t.advance()
                self.compileExpression()


                self.t.advance()
                if self.t.token == "]":
                    self.output()
                    self.w.write_a("+")
                    self.w.write_pop("pointer", 1)
                    self.w.write_push("that", 0)


            # subroutineName(expressionList)
            elif self.t.token == "(":
                self.output()

                self.t.advance()
                self.compileExpressionList()

                self.t.advance()
                if self.t.token == ")":
                    self.output()

                self.i += 1
                self.w.write_push("pointer", 0)
                self.w.write_call(CompilationEngine.class_name + id, self.i)
                self.i = 0

            # (className | varName) . subroutineName(expressionList)
            elif self.t.token == ".":
                self.output()

                #subroutineName
                self.t.advance()
                if self.is_identifier():
                    self.output()
                    sub = self.t.token

                if (id in self.st.class_table) or (id in self.st.sub_table):
                    self.i += 1
                    fn_name = self.st.type_of(id) + "." + sub
                    kind = self.st.kind_of(id)
                    index = self.st.index_of(id)
                    self.w.write_push(kind, index)
                else:
                    fn_name = id + "." + sub

                #case (
                self.t.advance()
                if self.t.token == "(":
                    self.output()

                self.t.advance()
                self.compileExpressionList()

                #case )
                if self.t.token == ")":
                    self.output()

                if (id in self.st.class_table) or (id in self.st.sub_table):
                    self.i += 1
                    fn_name = self.st.type_of(id) + "." + sub
                    kind = self.st.kind_of(id)
                    index = self.st.index_of(id)
                    self.w.write_push(kind, index)
                else:
                    fn_name = id + "." + sub
                self.w.write_call(fn_name, self.i)
                self.i = 0


            #varName
            else:
                self.t.token = temp_t

                kind = self.st.kind_of(self.t.token)
                index = self.st.index_of(self.t.token)
                self.w.write_push(kind, index)

                self.t.i = temp_i

        #case (expression)
        elif self.t.token == "(":
            self.output()

            self.t.advance()
            self.compileExpression()

            self.t.advance()
            if self.t.token == ")":
                self.output()

        #case integerConstant
        elif self.t.tokenType() == "INT_CONST":
            self.w.write_push("constant", int(self.t.token))
            self.output()

        #case stringConstant
        elif self.t.tokenType() == "STRING_CONST":
            s = self.t.token[1:-1]
            self.w.write_push("constant", len(s))
            self.w.write_call("String.new", 1)
            for c in s:
                self.w.write_push("constant", ord(c))
                self.w.write_call("String.appendChar", 2)

            self.output()

        #case true or false or null or this
        elif self.t.token in ["true", "false", "null", "this"]:
            t = self.t.token
            if t == "true":
                self.w.write_push("constant", 1)
                self.w.write_sa("-")
            elif t == "false" or t == "null":
                self.w.write_push("constant", 0)
            else: #this
                self.w.write_push("pointer", 0)
            self.output()

        #case unaryOp term
        elif self.t.token in ["-", "~"]:
            self.output()
            op = self.t.token

            self.t.advance()
            self.compileTerm()
            self.w.write_sa(op)


        else:
            raise Exception("invalid term")

        self.o += "</term>\n"
        return

    def compileExpression(self):
        self.o += "<expression>\n"
        #term
        self.compileTerm()

        while True:
            #op
            if self.next_token() in ["+", "-", "*", "/", "&", "|", "<", ">", "="]:
                self.t.advance()
                self.output()
                op = self.t.token
                self.t.advance()
                #term
                self.compileTerm()

                self.w.write_a(op)


            else:
                break

        self.o += "</expression>\n"
        return




    def compileExpressionList(self):
        self.o += "<expressionList>\n"

        if self.t.token == ")":
            self.o += "</expressionList>\n"
            return

        self.compileExpression()
        self.i += 1

        #the next token should be "," or ")"
        while True:
            self.t.advance()
            if self.t.token == ",":
                self.output()

                self.t.advance()
                self.compileExpression()
                self.i += 1

            elif self.t.token == ")":
                self.o += "</expressionList>\n"
                return

            else:
                raise Exception(self.o)






    def compileSubroutineBody(self):
        self.o += "<subroutineBody>\n"


        if self.t.token == "{":
            self.output()
        else:
            raise Exception()


        while True:
            if self.next_token() == "var":
                self.t.advance()
                self.compileVarDec()
            elif self.next_token() in ["let", "if", "while", "do", "return"]:
                break
            else:
                raise Exception()

        sub = CompilationEngine.sub_kind

        nvar = self.st.var_count("local")

        self.w.write_f(CompilationEngine.class_name + "." + CompilationEngine.sub_name, nvar)


        if sub == "constructor":
            self.w.write_push("constant", self.st.var_count("field"))
            self.w.write_call("Memory.alloc", 1)
            self.w.write_pop("pointer", 0)

        elif sub == "method":
            self.w.write_push("argument", 0)
            self.w.write_pop("pointer", 0)

        self.compileStatements()

        self.aieo("}")

        self.o += "</subroutineBody>\n"




    def compileStatements(self):
        self.o += "<statements>\n"
        while True:
            if self.next_token() == "let":
                self.t.advance()
                self.compileLet()
            elif self.next_token() == "if":
                self.t.advance()
                self.compileIf()
            elif self.next_token() == "while":
                self.t.advance()
                self.compileWhile()
            elif self.next_token() == "do":
                self.t.advance()
                self.compileDo()
            elif self.next_token() == "return":
                self.t.advance()
                self.compileReturn()
            else:
                self.o += "</statements>\n"
                return

    def compileReturn(self):
        self.o += "<returnStatement>\n"
        self.output()
        self.t.advance()

        #case return;
        if self.t.token == ";":
            self.output()
            self.w.write_push("constant", 0)
            self.w.write_r()
            self.o += "</returnStatement>\n"
            return
        #case expression return;
        else:
            self.compileExpression()

        self.t.advance()
        if self.t.token == ";":
            self.output()
            self.w.write_r()
            self.o += "</returnStatement>\n"
            return
        else:
            raise Exception(self.o)






    def compileLet(self):
        #'let' varName([expression])? '=' expression ';'
        self.o += "<letStatement>\n"
        #'let'
        self.output()

        is_array = False

        #varName
        self.t.advance()
        if self.is_identifier():
            name = self.t.token
            kind = self.st.kind_of(name)
            index = self.st.index_of(name)
            self.output()

        #case varName[expression]
        self.t.advance()
        if self.t.token == "[":
            is_array = True
            self.output()

            self.w.write_push(kind, index)

            self.t.advance()
            self.compileExpression()

            self.w.write_a("+")

            self.aieo("]")

            self.t.advance()

        if self.t.token == "=":
            self.output()

            #expression2
            self.t.advance()
            self.compileExpression()

            if is_array:
                self.w.write_pop("temp", 0)
                self.w.write_pop("pointer", 1)
                self.w.write_push("temp", 0)
                self.w.write_pop("that", 0)
            else:
                self.w.write_pop(kind, index)




            self.aieo(";")

            self.o += "</letStatement>\n"
            return

        else:
            raise Exception(self.o)

    def compileDo(self):
        #'do' subroutineCall
        self.o += "<doStatement>\n"
        self.output()

        self.t.advance()
        #subroutineName or (className | varName)
        if self.is_identifier():
            self.output()
            fn_name = self.t.token
            is_method = False
            #case (className | varName) . subroutineName
            self.t.advance()
            if self.t.token == ".":

                self.output()

                self.t.advance()
                if self.is_identifier():
                    self.output()
                    #case varName
                    if not self.st.kind_of(fn_name) == "none":
                        kind = self.st.kind_of(fn_name)
                        i = self.st.index_of(fn_name)
                        type = self.st.type_of(fn_name)
                        self.w.write_push(kind, i)
                        fn_name = type + "." + self.t.token
                        is_method = True
                    #case className (function call)
                    else:
                        fn_name = fn_name + "." + self.t.token
                    self.t.advance()
            #case subroutineName()
            else:
                fn_name = CompilationEngine.class_name + "." + fn_name
                self.w.write_push("pointer", 0)
                is_method = True





            if self.t.token == "(":
                self.output()
                self.t.advance()
                self.compileExpressionList()
            else:
                raise Exception(self.o)

            self.output()

            if is_method:
                self.i += 1


            self.w.write_call(fn_name, self.i)
            self.i = 0

            self.aieo(";")

            self.w.write_pop("temp", 0)

            self.o += "</doStatement>\n"
            return

        else:
            raise Exception(self.o)

    def compileIf(self):
        #'if'(expression){statements} ('else'{statements})?
        CompilationEngine.label_i += 1
        label1 = "L" + str(CompilationEngine.label_i)
        CompilationEngine.label_i += 1
        label2 = "L" + str(CompilationEngine.label_i)

        self.o += "<ifStatement>\n"
        self.output()

        self.aieo("(")

        self.t.advance()
        self.compileExpression()

        self.aieo(")")

        self.w.write_sa("~")
        self.w.write_if(label1)

        self.aieo("{")

        self.compileStatements()

        self.aieo("}")

        self.w.write_goto(label2)
        self.w.write_label(label1)

        if self.next_token() == "else":
            self.t.advance()
            self.output()

            self.aieo("{")

            self.compileStatements()

            self.aieo("}")

        self.w.write_label(label2)

        self.o += "</ifStatement>\n"

        return

    def compileVarDec(self):
        # 'var' type varName (',' varName)* ';'
        self.o += "<varDec>\n"
        if self.t.token == "var":
            self.output()
            kind = "local"
        else:
            raise Exception(self.o)

        #type
        self.t.advance()
        if self.is_type():
            self.output()
            type = self.t.token

        #varName
        self.t.advance()
        if self.is_identifier():
            self.output()
            name = self.t.token
            self.st.define(name, type, kind)

        self.t.advance()
        while not(self.t.token == ";"):
            if self.t.token == ",":
                self.output()

            #varName
            self.t.advance()
            if self.is_identifier():
                self.output()
                name = self.t.token
                self.st.define(name, type, kind)

            self.t.advance()

        self.output()

        self.o += "</varDec>\n"

        return

    def compileWhile(self):

        CompilationEngine.label_i += 1
        label1 = "L" + str(CompilationEngine.label_i)
        CompilationEngine.label_i += 1
        label2 = "L" + str(CompilationEngine.label_i)

        self.o += "<whileStatement>\n"
        self.output()

        self.w.write_label(label1)



        self.aieo("(")

        self.t.advance()
        self.compileExpression()

        self.aieo(")")

        self.w.write_sa("~")
        self.w.write_if(label2)

        self.aieo("{")

        self.compileStatements()

        self.aieo("}")

        self.w.write_goto(label1)
        self.w.write_label(label2)

        self.o += "</whileStatement>\n"

        return




    def is_statement(self):
        return self.t.token in ["let", "if", "while", "do", "return"]

    def next_token(self):
        temp_t = self.t.token
        temp_i = self.t.i

        self.t.advance()

        token = self.t.token

        self.t.token = temp_t
        self.t.i = temp_i

        return token

    def aieo(self, s):
        self.t.advance()
        if self.t.token == s:
            self.output()
        else:
            s = "\"" + s + "\""
            raise Exception(self.o)

    def is_op(self):
        return self.t.token in ["+", "-", "*", "/", "&", "|", "<", ">", "="]

    def is_identifier(self):
        return self.t.tokenType() == "IDENTIFIER"

    def is_type(self):
        return (self.t.token in ["int", "char", "boolean"]) or (self.t.tokenType() == "IDENTIFIER")

    def output(self):
        self.o += self.t.tag()
